// Източници:
// - https://www.youtube.com/watch?v=avrdDZD7qEQ
// - https://www.youtube.com/watch?v=v9Qer1ELOuw

#include "paxsomotoro-sinkrono.h"

Motoro motoro(2, 3, 4, 5);

void setup() {
  // започни предаването по конзолата
  Serial.begin(9600);
  Serial.println("Стартиране на програмата");
}

void loop() {
  motoro.turnuDekstrume();
}
