class Motoro {
  private:
    byte pingloj[4];
    byte lastaFiksitaPozicio;
    
    void aktiviguMagnetoj(bool a, bool b, bool c, bool d) {
      digitalWrite(pingloj[0], a);
      digitalWrite(pingloj[1], b);
      digitalWrite(pingloj[2], c);
      digitalWrite(pingloj[3], d);
    }
    
  public:
    // конструктори
    // TODO: да намеря начин да го махна
    // в момента стои тук, защото компилаторът не ми позволява да създам обект от клас шаси,
    // който съдържа поле от клас мотор, когато този клас няма конструктор без параметри
    Motoro() {}
    
    Motoro(int a, int b, int c, int d) {
      pingloj[0] = a;
      pingloj[1] = b;
      pingloj[2] = c;
      pingloj[3] = d;

      // задай всички пинове като изходящи
      for (int i = 0; i < 4; i++) {
        pinMode(pingloj[i], OUTPUT);
      }

      lastaFiksitaPozicio = -1;
    }

    // задействане на магнит
    void stariguPozicio(byte pozicio) {
      switch(pozicio) {
        case 0:  aktiviguMagnetoj(1, 0, 0, 0); break;
        case 1:  aktiviguMagnetoj(1, 1, 0, 0); break;
        case 2:  aktiviguMagnetoj(0, 1, 0, 0); break;
        case 3:  aktiviguMagnetoj(0, 1, 1, 0); break;
        case 4:  aktiviguMagnetoj(0, 0, 1, 0); break;
        case 5:  aktiviguMagnetoj(0, 0, 1, 1); break;
        case 6:  aktiviguMagnetoj(0, 0, 0, 1); break;
        case 7:  aktiviguMagnetoj(1, 0, 0, 1); break;
        default: aktiviguMagnetoj(0, 0, 0, 0); break;
      }
      lastaFiksitaPozicio = pozicio;
      // TODO: да го направя с проверка за милисекундите от последното придвижване,
      // за да може робота да върши и други неща, вместо да е просто блокиран в чакане
      delay(500);
    }

    // взема номера на следващия магнит по ред
    byte ricevuSekvan() {
      return (lastaFiksitaPozicio == 7 || lastaFiksitaPozicio == -1) ? 0 : lastaFiksitaPozicio + 1;
    }

    // взема номера на предишния по ред магнит
    byte ricevuAntaxuan() {
      return (lastaFiksitaPozicio == 0 || lastaFiksitaPozicio == -1) ? 7 : lastaFiksitaPozicio - 1;
    }

    // завърта се веднъж по часовниковата стрелка
    void turnuDekstrume() {
      stariguPozicio(ricevuSekvan());
    }

    // завърта се веднъж обратно на часовниковата стрелка
    void turnuMaldekstrume() {
      stariguPozicio(ricevuAntaxuan());
    }

    // изключва всички магнити, за да не се загрява моторът
    void ripozi() {
      stariguPozicio(-1);
    }
};
