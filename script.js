let c = document.getElementById("myCanvas");
let ctx = c.getContext("2d");
c.width = 3800;
c.height = 1200;

function getNoteHue(note) {
  switch(note) {
    case 0:  return 45; // A
    case 1: return 60; // A#
    case 2: return 87; // B
    case 3:  return 138; // C
    case 4:  return 164; // C#
    case 5:  return 224; // D
    case 6:  return 264; // D#
    case 7:  return 300; // E
    case 8:  return 322; // F
    case 9:  return 5; // F#
    case 10:  return 22; // G
    case 11:  return 30; // G#
  }
}

function getNoteLightness(octave) {
  switch(octave) {
    case 4: return 35;
    case 5: return 55;
    case 6: return 80;
  }
}

function getNoteLetter(note) {
  switch(note) {
    case 0:  return "A";
    case 1: return "A#";
    case 2: return "B";
    case 3:  return "C";
    case 4:  return "C#";
    case 5:  return "D";
    case 6:  return "D#";
    case 7:  return "E";
    case 8:  return "F";
    case 9:  return "F#";
    case 10:  return "G";
    case 11:  return "G#";
  }
}

function getNoteName(note) {
  switch(note) {
    case 0:  return "ла";
    case 1: return "ла#";
    case 2: return "си";
    case 3:  return "до";
    case 4:  return "до#";
    case 5:  return "ре";
    case 6:  return "ре#";
    case 7:  return "ми";
    case 8:  return "фа";
    case 9:  return "фа#";
    case 10:  return "сол";
    case 11:  return "сол#";
  }
}

function buildAllNotes() {
  let notes = [];
  for (let i = 0; i < 100; i++) {
    let num = i % 12;
    let octave = Math.floor(i / 12);
    let freq = Math.round(440*Math.pow(2,i/12-6));
    var hue = getNoteHue(num);
    let saturation = 40;
    let lightness = getNoteLightness(octave);
    let color = "hsl(" + hue + ", " + saturation + "%, " + lightness +"%)";
    let letter = getNoteLetter(num);
    let name = getNoteName(num);
    notes[i] = {num: num, letter: letter, name: name, octave: octave, freq: freq, color: color};
  }
  return notes;
}

function buildNotesOnAString(zeroNote) {
  let allNotes = buildAllNotes();
  let stringNotes = [];
  for (let i = 0; i <= 18; i++) {
    stringNotes[i] = allNotes[i + zeroNote];
  }
  return stringNotes;
}

function buildNotesOnTheGuitarNeck() {
  return [
    buildNotesOnAString(48),
    buildNotesOnAString(51),
    buildNotesOnAString(54),
    buildNotesOnAString(57),
    buildNotesOnAString(60),
    buildNotesOnAString(63),
  ];
}

function drawNote(note, numOnTheString, stringNumber) {
  let width = 200;
  let height = 200;
  let posx = numOnTheString * width;
  let posy = stringNumber * height;
  let textPosXLeft = posx + 25;
  let textPosXCenter = posx + width / 2;
  let textPosXRight = posx + width - 25;
  let textPosYTop = posy + 50;
  let textPosYCenter = posy + 130;
  let textPosYBottom = posy + height - 25;
  ctx.fillStyle = note.color;
  ctx.fillRect(posx, posy, width, height);
  ctx.strokeRect(posx, posy, width, height);

  // номер
  ctx.font = "80px Sans";
  ctx.fillStyle = "black";
  ctx.textAlign = "center"; 
  ctx.fillText(note.num, textPosXCenter, textPosYCenter);

  // латинска буква
  ctx.font = "32px Sans";
  ctx.textAlign = "left"; 
  ctx.fillText(note.letter, textPosXLeft, textPosYTop);

  // честота
  ctx.fillText(note.freq, textPosXLeft, textPosYBottom);

  // име на български
  ctx.textAlign = "right"; 
  ctx.fillText(note.name, textPosXRight, textPosYTop);

  // октава
  ctx.fillText(note.octave, textPosXRight, textPosYBottom);
}

function drawNotes() {
  let notesOnGuitar = buildNotesOnTheGuitarNeck();
  let stringNumber = 0;
  let noteNumberOnTheString = 0;
  for (let notesOnString of notesOnGuitar) {
    for (let note of notesOnString) {
      drawNote(note, noteNumberOnTheString, stringNumber);
      noteNumberOnTheString++;
    }
    stringNumber++;
    noteNumberOnTheString = 0;
  }
}

drawNotes();
