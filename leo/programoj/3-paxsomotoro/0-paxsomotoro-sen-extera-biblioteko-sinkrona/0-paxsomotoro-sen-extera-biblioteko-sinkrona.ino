// Източници:
// - https://www.youtube.com/watch?v=avrdDZD7qEQ
// - https://www.youtube.com/watch?v=v9Qer1ELOuw

#import "steppermotors.h"
#import "chasis.h"

Motor motorLeft(8, 9, 10, 11);
Motor motorRight(4, 5, 6, 7);
Chassis chassis(motorLeft, motorRight);

void setup() {
	for (int i = 0; i < 4; i++) {
		chassis.forward(50); // напред с 50 единици
  	chassis.turnRight(1/4.0); // надясно 1/4 от окръжността
	}
}

void loop() {}
