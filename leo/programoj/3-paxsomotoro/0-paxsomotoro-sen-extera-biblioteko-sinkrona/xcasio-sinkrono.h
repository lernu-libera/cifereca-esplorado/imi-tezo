class Xcasio {
  private:
    Motoro maldekstra;
    Motoro dekstra;
    const int PAXSOJ_POR_UNU_TURNO_ANTAXUE_REDUKTO = 32;
    const int REDUKTO = 64;
    const int PAXSOJ_POR_UNU_TURNO_POST_REDUKTO = PAXSOJ_POR_UNU_TURNO_ANTAXUE_REDUKTO * REDUKTO;
    const int DIAMETRO_DE_PNEXOJ = 64;
    const float CIRKONFERENCO_DE_PNEXOJ = 3.14159 * DIAMETRO_DE_PNEXOJ;
    const float PAXSOJ_POR_MILIMETRO = PAXSOJ_POR_UNU_TURNO_POST_REDUKTO / CIRKONFERENCO_DE_PNEXOJ;
    const int AKSDISTANCO = 155;
    const float CIRKONFERENCO_DE_LA_CIRKLO_ROBOTO = 3.14159 * AKSDISTANCO;
    const float PAXSOJ_POR_KOMPLETA_ROTACIO_DE_LA_ROBOTO = PAXSOJ_POR_MILIMETRO * CIRKONFERENCO_DE_LA_CIRKLO_ROBOTO;

  public:
    // конструктор
    Xcasio(Motoro m, Motoro d) {
      maldekstra = m;
      dekstra = d;
    }

    // завърти левия мотор в посока напред
    void turnuLaMaldekstranMotoroAntaxuen() {
      maldekstra.turnuDekstrume();
    }

    // завърти десния мотор в посока напред
    void turnuLaDekstranMotoroAntaxuen() {
      dekstra.turnuMaldekstrume();
    }

    // завърти левия мотор в посока назад
    void turnuLaMaldekstranMotoroReen() {
      maldekstra.turnuMaldekstrume();
    }

    // завърти десния мотор в посока назад
    void turnuLaDekstranMotoroReen() {
      dekstra.turnuDekstrume();
    }

    // придвижи се напред с две стъпки
    void antaxueniruEnDuPaxsoj() {
      turnuLaMaldekstranMotoroAntaxuen();
      turnuLaDekstranMotoroAntaxuen();
      turnuLaDekstranMotoroAntaxuen();
      turnuLaMaldekstranMotoroAntaxuen();
    }

    // придвижи се назад с две стъпки
    void retroiruEnDuPaxsoj() {
      turnuLaMaldekstranMotoroReen();
      turnuLaDekstranMotoroReen();
      turnuLaDekstranMotoroReen();
      turnuLaMaldekstranMotoroReen();
    }

    // завърти се наляво
    void turnuMaldekstren() {
      turnuLaMaldekstranMotoroReen();
      turnuLaDekstranMotoroAntaxuen();
    }

    // завърти се надясно
    void turnuDekstren() {
      turnuLaMaldekstranMotoroAntaxuen();
      turnuLaDekstranMotoroReen();
    }

    // придвижи се (напред или назад)
    void movu(int longeco, bool estasAntaxue) {
      float paxso = longeco * PAXSOJ_POR_MILIMETRO;
      int n = paxso / 2;
      for(int i = 0; i < n; i++) {
        estasAntaxue ? antaxueniruEnDuPaxsoj() : retroiruEnDuPaxsoj();
      }
    }

    // движа се напред
    void movuAntaxuen(int longeco) {
      movu(longeco, true);
    }

    // движа се назад
    void movuReen(int longeco) {
      movu(longeco, false);
    }

    // завърти се (наляво или надясно)
    void turnu(float anguloTao, bool estasMaldekstra) {
      int paxso = anguloTao * PAXSOJ_POR_KOMPLETA_ROTACIO_DE_LA_ROBOTO;
      for(int i = 0; i < paxso; i++) {
        estasMaldekstra ? turnuMaldekstren() : turnuDekstren();
      }
    }

    // завърти се наляво
    void turnuMaldekstren(float anguloTao) {
      turnu(anguloTao, true);
    }

    // завърти се надясно
    void turnuDekstren(float anguloTao) {
      turnu(anguloTao, false);
    }

    void ripozi() {
      maldekstra.ripozi();
      dekstra.ripozi();
    }
};
