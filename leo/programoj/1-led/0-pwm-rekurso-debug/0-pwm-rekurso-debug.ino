// източници:
// - https://docs.arduino.cc/learn/microcontrollers/analog-output
// - https://www.arduino.cc/reference/en/language/functions/analog-io/analogwrite/

// Тази програма си я написах сам.
// Не съм ползвал код от друго място като основа, освен документацията на Ардуино
// Целта ми беше да разбера по-добре рекурсията, да изпробвам как работи в Ардуино
// и да проверя дали мога да напиша програма за робота, чрез която да демонстрирам по-добре
// рекурсията, когато дойде време да я преподавам в училище след няколко седмици.

// Тази програма е същата като следващата по номер,
// но с много, много повече debug информация,
// която се извежда на екрана и би помогнала
// за по-доброто рабиране на рекурсията.

#define LED_PIN 9
#define PAXSO 10

#include <math.h>

// помощна функция, чрез която всяка от рекурсивните функции се представя коя е чрез серийния монитор
void presiKiuViEstas(short valoro) {
  // пресмята нивото си в йерархията
  byte n = round((float)valoro / PAXSO);

  // извежда толкова tab-а, колкото е нивото ѝ
  for (byte i = 0; i < n; i++) {
    Serial.print("\t");
  }

  // представя се
  Serial.print("Аз съм ");
  Serial.print(valoro);
  Serial.print(" ---> ");
}

void xsaltiLed(short valoro) {
  presiKiuViEstas(valoro);
  Serial.println("Задействам лампичката да свети с моята стойност и изчаквам една секунда.");
  analogWrite(LED_PIN, valoro);
  delay(1000);
}

// рекурсивна функция, която кара лампичката да увеличава и намалява яркостта си от 0 до 255 и обратно
short fade(short valoro) {
  presiKiuViEstas(valoro);
  Serial.println("Еха, току що се появих на бял свят!");
  
  if (valoro > 255) {
    presiKiuViEstas(valoro);
    Serial.println("Стойността ми е извън обхвата на analogWrite -> 1 байт (от 0 до 255).");

    presiKiuViEstas(valoro);
    Serial.print("Променям стойността си на ");
    valoro = 255;
    Serial.print(valoro);
    Serial.println(".");

    xsaltiLed(valoro);

    presiKiuViEstas(valoro);
    Serial.println("Няма да създавам никого след себе си, тъй като първоначалната ми стойност беше невалидна, следователно съм последната в тази йерархия.");
    
    presiKiuViEstas(valoro);
    Serial.println("Свърших работата, за която бях създадена. Прекратявам изпълнението си и връщам новата си стойност.");
    return valoro;
  }

  xsaltiLed(valoro);

  // пресметни стойността, с която да създадеш детето си
  short infanvaloro = valoro + PAXSO;
  
  presiKiuViEstas(valoro);
  Serial.print("Създавам ");
  Serial.print(infanvaloro);
  Serial.print(" по формулата ");
  Serial.print(valoro);
  Serial.print(" + ");
  Serial.println(PAXSO);

  // вземи стойността, която детето е върнало в края на изпълнението си
  infanvaloro = fade(infanvaloro);

  presiKiuViEstas(valoro);
  Serial.print("Получих резултата от изпълнението на ");
  Serial.print(infanvaloro);
  Serial.println(".");

  xsaltiLed(valoro);

  presiKiuViEstas(valoro);
  Serial.println("Свърших работата, за която бях създадена. Прекратявам изпълнението си и връщам стойността си.");

  return valoro;
}

void setup() {
  delay(500);
  Serial.begin(9400);
}

void loop() {
  fade(0);
  Serial.println();
}
