// източници:
// - https://www.youtube.com/watch?v=oQQpAACa3ac
// - https://www.youtube.com/watch?v=J61_PKyWjxU
// - https://www.arduino.cc/reference/en/language/functions/external-interrupts/attachinterrupt/
// електронната схема е много лесна: свъзвам с кабелче PIN2 и GND
// когато има промяна при протичащия ток през кабелчето (вкарвайки или изкарвайки го от някое от PIN-четата)
// се задейства прекъсването на нормалното протичане на програмата
// Вместо вкарване и изкарване на кабелчето, може да бъде ползвано ключе.

#define LED_PIN 13
#define INTERROMPO_PIN 2

volatile byte state = HIGH;

void setup() {
  pinMode(LED_PIN, OUTPUT);
  pinMode(INTERROMPO_PIN, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(INTERROMPO_PIN), changeState, FALLING);
}

void loop() {
  digitalWrite(LED_PIN, state);
}

void changeState() {
  state = !state;
}
