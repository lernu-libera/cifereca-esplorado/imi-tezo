// Тази скица служи за задействане на моторче чрез широчинно-импулсна модулация.
// Може да се ползва директно с моторчето за дрон (https://www.olimex.com/Products/Robot-CNC-Parts/Micro-Motors/MOTOR-F1607/).
// включено към ардуино, без външно захранване, транзистор и диод, тъй като Ардуино предоставя достатъчно количество ток,
// за да проработи моторчето (за разлика от останалите DC моторчета, които имам).
// С такова моторче може да се направи дрон:
// - https://www.youtube.com/watch?v=koSanik0-8I&feature=emb_imp_woyt
// - https://www.instructables.com/Micro-Quadcopter-in-OpenSCAD/
// - https://www.thingiverse.com/thing:4708493/files

void setup() {
  pinMode(3, OUTPUT);
}

void loop() {
  digitalWrite(3, HIGH);
  delay(10);
  digitalWrite(3, LOW);
  delay(100);  
}
