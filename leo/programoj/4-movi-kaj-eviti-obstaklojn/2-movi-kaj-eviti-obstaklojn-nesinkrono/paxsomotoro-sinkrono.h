class Motoro {
  private:
    byte pingloj[4];
    byte lastaAktivigitaMagneto;
    
  public:
    // конструктори
    // TODO: да намеря начин да го махна
    // в момента стои тук, защото компилаторът не ми позволява да създам обект от клас шаси,
    // който съдържа поле от клас мотор, когато този клас няма конструктор без параметри
    Motoro() {}
    
    Motoro(int a, int b, int c, int d) {
      pingloj[0] = a;
      pingloj[1] = b;
      pingloj[2] = c;
      pingloj[3] = d;

      // задай всички пинове като изходящи
      for (int i = 0; i < 4; i++) {
        pinMode(pingloj[i], OUTPUT);
      }
    }

    // задействане на магнит
    void aktiviguMagneton(byte pinglo) {
      switch(pinglo) {
        case 0:
          digitalWrite(pingloj[0], HIGH);
          digitalWrite(pingloj[1], LOW);
          digitalWrite(pingloj[2], LOW);
          digitalWrite(pingloj[3], LOW);
          lastaAktivigitaMagneto = pinglo;
          break;
        case 1:
          digitalWrite(pingloj[0], LOW);
          digitalWrite(pingloj[1], HIGH);
          digitalWrite(pingloj[2], LOW);
          digitalWrite(pingloj[3], LOW);
          lastaAktivigitaMagneto = pinglo;
          break;
        case 2:
          digitalWrite(pingloj[0], LOW);
          digitalWrite(pingloj[1], LOW);
          digitalWrite(pingloj[2], HIGH);
          digitalWrite(pingloj[3], LOW);
          lastaAktivigitaMagneto = pinglo;
          break;
        case 3:
          digitalWrite(pingloj[0], LOW);
          digitalWrite(pingloj[1], LOW);
          digitalWrite(pingloj[2], LOW);
          digitalWrite(pingloj[3], HIGH);
          lastaAktivigitaMagneto = pinglo;
          break;
        default:
          digitalWrite(pingloj[0], LOW);
          digitalWrite(pingloj[1], LOW);
          digitalWrite(pingloj[2], LOW);
          digitalWrite(pingloj[3], LOW);
          lastaAktivigitaMagneto = -1;
      }
      // TODO: да го направя с проверка за милисекундите от последното придвижване,
      // за да може робота да върши и други неща, вместо да е просто блокиран
      // за 2 милисекунди
      delay(2);
    }

    // взема номера на следващия магнит по ред
    byte ricevuSekvan() {
      return (lastaAktivigitaMagneto == 3 || lastaAktivigitaMagneto == -1) ? 0 : lastaAktivigitaMagneto + 1;
    }

    // взема номера на предишния по ред магнит
    byte ricevuAntaxuan() {
      return (lastaAktivigitaMagneto == 0 || lastaAktivigitaMagneto == -1) ? 3 : lastaAktivigitaMagneto - 1;
    }

    // завърта се веднъж по часовниковата стрелка
    void turnuDekstrume() {
      aktiviguMagneton(ricevuSekvan());
    }

    // завърта се веднъж обратно на часовниковата стрелка
    void turnuMaldekstrume() {
      aktiviguMagneton(ricevuAntaxuan());
    }

    // изключва всички магнити, за да не се загрява моторът
    void ripozi() {
      aktiviguMagneton(-1);
    }
};
