// Клас за опашка от команди, имплементирана чрез едносвързан списък

class KomandoVico {
  private:
    KomandoVicoNodo iniciala;
    KomandoVicoNodo lasta;

  public:
    KomandoVico() {
      iniciala = NULL;
      lasta = NULL;
    }
    
    void aligu(Komando komando) {
      KomandoVicoNodo vicoNodo(komando);
      if (iniciala == NULL) {
        // списъкът е празен, затова добавяме новият елемент и като първи
        iniciala = vicoNodo;
      } else {
        // вече има елементи в списъка, затова закачаме нови елемент към последния до момента
        lasta.sekvaRef = &vicoNodo;
      }

      // задаваме новия елемент като последен
      lasta = vicoNodo;
    }
    
    void ekzekutu() {
      if (iniciala.estasFinita()) {
        iniciala = *(iniciala.sekvaRef);
      } else {
        iniciala.ekzekutu();
      }
    }

    void printVico() {
      if (iniciala == NULL) return;
      KomandoVicoNodo nodo = iniciala;
      while(nodo.sekvaRef) {
        nodo.printNode();
        nodo = *(nodo.sekvaRef);
      }
    }
};
