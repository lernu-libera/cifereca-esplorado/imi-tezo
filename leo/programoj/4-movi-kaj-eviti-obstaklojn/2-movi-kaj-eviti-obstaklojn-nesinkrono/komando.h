// ma - movu antauen - върви напред
// mr - movu reen - върви назад
// tm - turnu maldekstren - завърти се наляво
// td - turnu dekstren - завърти се надясно
// r - ripozi - почивай си

class Komando {
  public:
    byte kio;       // какво
    int kiom;       // колко
    unsigned long tempoDeLaSekvaPulso; // времето, когато искаме да бъде изпратен следващия пулс
    Xcasio xcasio;  // шасито, което ще се движи
    int ELLASILON_INTERVALO = 20; // минималното време, през което искаме да се придвижва мотора, за да сме сигурни, че роторът е имал достатъчно време да се придвижи
  
    Komando() {}
    Komando(int o, int om, Xcasio xc) {
      kio = o;
      kiom = om;
      xcasio = xc;
      tempoDeLaSekvaPulso = millis();
    }

    bool estasFinita() {
      return kiom <= 0;
    }
    
    void ekzekutu() {
      // ако все още не е време за изпълнение на командата, прекрати изпълнението на функцията
      if (millis() < tempoDeLaSekvaPulso) {
        return;
      }

      if (kio == 1) xcasio.movuAntaxuen();
      else if (kio == 2) xcasio.movuReen();
      else if (kio == 3) xcasio.turnuMaldekstren();
      else if (kio == 4) xcasio.turnuDekstren();
      else if (kio == 5) xcasio.ripozi();
      else Serial.println("Грешка");
      
      kiom--;
      tempoDeLaSekvaPulso = millis() + ELLASILON_INTERVALO;
    }

    void printNode() {
      Serial.print(kio);
      Serial.print(" ");
      Serial.print(kiom);
      Serial.print(" ");
      Serial.print(tempoDeLaSekvaPulso);
      Serial.println();
    }
};
