// Клас за елемент от опашката от команди.
// Всеки такъв елемент съдържа командата си и указател към следващата команда,
// за да могат да бъдат навързани в едносвързан списък

class KomandoVicoNodo : public Komando {
  public:
    KomandoVicoNodo* sekvaRef;
    
    KomandoVicoNodo(Komando k) {
      kio = k.kio;
      kiom = k.kiom;
      xcasio = k.xcasio;
      sekvaRef = NULL;
    }
};
