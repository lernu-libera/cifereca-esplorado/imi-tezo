// Източници:
// - https://www.youtube.com/watch?v=avrdDZD7qEQ
// - https://www.youtube.com/watch?v=v9Qer1ELOuw

#include "sonaro-nesinkrono.h"
#include "paxsomotoro-sinkrono.h"
#include "xcasio-nesinkrono.h"
#include "komando.h"
#include "komando-vico-nodo.h"
#include "komando-vico.h"

Motoro motoroMaldekstra(8, 9, 10, 11);
Motoro motoroDekstra(4, 5, 6, 7);
Xcasio xcasio(motoroMaldekstra, motoroDekstra);
Sonaro sonaro(2, 3);
KomandoVico komandoj;

void setup() {
  // започни предаването по конзолата
  Serial.begin(9600);
  Serial.println("Стартиране на програмата");

  Komando k1(1, 900, xcasio);
  Komando k2(2, 400, xcasio);
  Komando k3(3, 600, xcasio);
  
  komandoj.aligu(k1);
  komandoj.aligu(k2);
  komandoj.aligu(k3);

  komandoj.printVico();
}

void loop() {
  
}
