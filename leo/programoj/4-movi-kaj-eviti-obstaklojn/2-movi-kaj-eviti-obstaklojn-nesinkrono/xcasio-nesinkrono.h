class Xcasio {
  private:
    Motoro maldekstra;
    Motoro dekstra;
    
    int PAXSOJ_POR_UNU_TURNO_ANTAXUE_REDUKTO = 32;
    int REDUKTO = 64;
    int PAXSOJ_POR_UNU_TURNO_POST_REDUKTO = PAXSOJ_POR_UNU_TURNO_ANTAXUE_REDUKTO * REDUKTO;
    int DIAMETRO_DE_PNEXOJ = 64;
    float CIRKONFERENCO_DE_PNEXOJ = 3.14159 * DIAMETRO_DE_PNEXOJ;
    float PAXSOJ_POR_MILIMETRO = PAXSOJ_POR_UNU_TURNO_POST_REDUKTO / CIRKONFERENCO_DE_PNEXOJ;
    int AKSDISTANCO = 155;
    float CIRKONFERENCO_DE_LA_CIRKLO_ROBOTO = 3.14159 * AKSDISTANCO;
    float PAXSOJ_POR_KOMPLETA_ROTACIO_DE_LA_ROBOTO = PAXSOJ_POR_MILIMETRO * CIRKONFERENCO_DE_LA_CIRKLO_ROBOTO;

  public:
    // конструктори
    Xcasio() {}
    
    Xcasio(Motoro m, Motoro d) {
      maldekstra = m;
      dekstra = d;
    }

    // завърти левия мотор в посока напред
    void turnuLaMaldekstranMotoroAntaxuen() {
      maldekstra.turnuDekstrume();
    }

    // завърти десния мотор в посока напред
    void turnuLaDekstranMotoroAntaxuen() {
      dekstra.turnuMaldekstrume();
    }

    // завърти левия мотор в посока назад
    void turnuLaMaldekstranMotoroReen() {
      maldekstra.turnuMaldekstrume();
    }

    // завърти десния мотор в посока назад
    void turnuLaDekstranMotoroReen() {
      dekstra.turnuDekstrume();
    }

    // придвижи се напред с една стъпка
    void movuAntaxuen() {
      turnuLaMaldekstranMotoroAntaxuen();
      turnuLaDekstranMotoroAntaxuen();
    }

    // придвижи се назад с една стъпка
    void movuReen() {
      turnuLaMaldekstranMotoroReen();
      turnuLaDekstranMotoroReen();
    }

    // завърти се наляво
    void turnuMaldekstren() {
      turnuLaMaldekstranMotoroReen();
      turnuLaDekstranMotoroAntaxuen();
    }

    // завърти се надясно
    void turnuDekstren() {
      turnuLaMaldekstranMotoroAntaxuen();
      turnuLaDekstranMotoroReen();
    }

    // придвижи се (напред или назад)
    void movu(int longeco, bool estasAntaxue) {
      int n = longeco * PAXSOJ_POR_MILIMETRO;
      for(int i = 0; i < n; i++) {
        estasAntaxue ? movuAntaxuen() : movuReen();
      }
    }

    // движа се напред
    void movuAntaxuen(int longeco) {
      movu(longeco, true);
    }

    // движа се назад
    void movuReen(int longeco) {
      movu(longeco, false);
    }

    // завърти се (наляво или надясно)
    void turnu(float anguloTao, bool estasMaldekstra) {
      int paxso = anguloTao * PAXSOJ_POR_KOMPLETA_ROTACIO_DE_LA_ROBOTO;
      for(int i = 0; i < paxso; i++) {
        estasMaldekstra ? turnuMaldekstren() : turnuDekstren();
      }
    }

    // завърти се наляво
    void turnuMaldekstren(float anguloTao) {
      turnu(anguloTao, true);
    }

    // завърти се надясно
    void turnuDekstren(float anguloTao) {
      turnu(anguloTao, false);
    }

    void ripozi() {
      maldekstra.ripozi();
      dekstra.ripozi();
    }
};
