#include <iostream>
#include "node.cpp"
#include "linked-commands-queue.cpp"
using namespace std;

int main() {
  Node n1;
  n1.command = f;
  n1.repetitions = 10;

  Node n2;
  n2.command = s;
  n2.repetitions = 5;

  Node n3;
  n3.command = b;
  n3.repetitions = 2;

  Node n4;
  n4.command = b;
  n4.repetitions = 5;

  LinkedCommandsQueue l;
  l.append(&n1);
  l.append(&n2);
  l.append(&n3);
  l.append(&n4);

  while (l.hasNext()) {
    cout << l.pullNextCommand();
  }
  return 0;
}
