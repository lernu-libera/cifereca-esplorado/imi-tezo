class LinkedCommandsQueue {
  private:
    Node* initial;

    Node* getLast() {
      if (!initial) {
        return 0;
      }

      Node* n = initial;
      while ((*n).next) {
        n = (*n).next;
      }
      return n;
    }

  public:
    void append(Node* n) {
      if (!initial) {
        initial = n;
        return;
      }

      Node* last = getLast();
      bool isLastCommandTheSame = (*last).command == (*n).command;
      if (isLastCommandTheSame) {
        (*last).repetitions += (*n).repetitions;
      } else {
        (*last).next = n;
      }
    }

    bool hasNext() {
      return !!initial;
    }

    Command pullNextCommand() {
      if (!initial) {
        return s;
      }

      Command c = (*initial).command;
      ((*initial).repetitions)--;

      bool isFirstCommandOver = ((*initial).repetitions) == 0;
      if (isFirstCommandOver) {
        initial =  (*initial).next;
      }
      return c;
    }
};
