enum Command {f, b, l, r, s};

class Node {
  public:
    Command command;
    int repetitions;
    Node* next;
};
