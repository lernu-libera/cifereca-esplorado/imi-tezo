// Източници:
// - https://www.youtube.com/watch?v=avrdDZD7qEQ
// - https://www.youtube.com/watch?v=v9Qer1ELOuw

#include "paxsomotoro-sinkrono.h"
#include "xcasio-sinkrono.h"
#include "sonaro-nesinkrono.h"

Motoro motoroMaldekstra(8, 9, 10, 11);
Motoro motoroDekstra(4, 5, 6, 7);
Xcasio xcasio(motoroMaldekstra, motoroDekstra);
Sonaro sonaro(2, 3);

void setup() {
  // започни предаването по конзолата
  Serial.begin(9600);
  Serial.println("Стартиране на програмата");
}

void loop() {
  int distanco = sonaro.akiriDistanco();
  Serial.println(distanco);
  if (distanco > 20) {
    xcasio.movuAntaxuen(10);
  } else {
    xcasio.turnuDekstren(1 / 50.0);
  }
}
