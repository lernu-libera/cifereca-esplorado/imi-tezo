class Sonaro {
  private:
    byte ellasionPin; // пинът за изпращане на ултразвук
    byte exhoPin; // пинът за получаване на ехото
    unsigned long antaxuaUltrasonoEllasilonTempo = 0; // времето на последното ехолокиране със сонара (в милисекунди спрямо стартирането на програмата)
    int distanco = -1; // разстоянието между робота и препятствието пред него (в сантиметри); -1 означава, че препятствието е извън обхвата на сонара (от 2 до 400 сантиметра)
    
    const int ULTRASONO_ELLASILON_INTERVALO = 200; // минималното време, през което искаме да се задейства ехолокация
    
    int mezuruDistanco() {
      // изпрати към сонара сигнал с продължителност 1 / 100 000 от секундата
      digitalWrite(ellasionPin, HIGH);  // започни изпращането на сигнал
      delayMicroseconds(10);            // изчакай 1 / 100 000 част от секундата
      digitalWrite(ellasionPin, LOW);   // спри изпращането на сигнал
      
      // измери продължителността на отговора от сонара
      int dt = pulseIn(exhoPin, HIGH); // продължителността в микросекунди
      // пресметни разстоянието, което е пропътувал звука
      const float RAPIDO_DE_SONO = 0.0343;  // скорост на звука в сантиметри за микросекунда
      float s2 = dt * RAPIDO_DE_SONO;       // пътят в сантиметри, който е изминал звукът отивайки към препятствието и връщайки се обратно до сонара
      int s = s2 / 2;                       // пътят само в едната посока
      bool estasEnLaLimojDeLaSensilo = (s > 2 && s < 400);  // измерването в рамките на ограниченията на сензора ли е?
      
      // връщане на резултата
      return estasEnLaLimojDeLaSensilo ? s : -1; // върни стойността, ако е валидна и -1 в противен случай
    }

  public:
    Sonaro(byte ella, byte exho) {
      ellasionPin = ella;
      exhoPin = exho;

      // задай пиновете за изход и вход на сонара
      pinMode(ellasionPin, OUTPUT);
      pinMode(exhoPin, INPUT);
    }
    
    int akiriDistanco() {
      // пресмятаме изминалото време от последната ехолокация до сега
      unsigned long nun = millis(); // времето от стартирането на програмата до сега (в милисекунди)
      unsigned long dt = nun - antaxuaUltrasonoEllasilonTempo; // изминалото време от последната ехолокация до сега
      
      // задействаме нова ехолокация, ако минималното време вече е изтекло
      if (dt >= ULTRASONO_ELLASILON_INTERVALO) {
        antaxuaUltrasonoEllasilonTempo = nun;
        int distancoProvizora = mezuruDistanco(); // временна променлива, в която записваме измереното разстояние
        if (distancoProvizora != -1) {
          distanco = distancoProvizora;
        }
      }

      return distanco;
    }
};
