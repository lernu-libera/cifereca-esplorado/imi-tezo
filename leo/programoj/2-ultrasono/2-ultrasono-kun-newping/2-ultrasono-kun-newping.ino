#define ULTRASONO_ELLASILON_PIN 12
#define ULTRASONO_EXHO_PIN 13
#define ULTRASONO_MAKS_DISTANCO 400

#include <NewPing.h>

NewPing sonar(ULTRASONO_ELLASILON_PIN, ULTRASONO_EXHO_PIN, ULTRASONO_MAKS_DISTANCO); // създаваме сонар обекта

unsigned long antaxuaUltrasonoEllasilonTempo = 0; // времето на последното ехолокиране със сонара (в милисекунди спрямо стартирането на програмата)
int distanco; // разстоянието между робота и препятствието пред него (в сантиметри); -1 означава, че препятствието е извън обхвата на сонара (от 2 до 400 сантиметра)

int mezuruDistancon() { // Timer2 interrupt извиква тази функция на всеки 24 микросекунди ... не разбирам защо и как се случва това. Кодът копирах и промених от: https://bitbucket.org/teckel12/arduino-new-ping/wiki/Home
  if (sonar.check_timer()) {
    int s = sonar.ping_result / US_ROUNDTRIP_CM;          // пътят в едната посока (в сантиметри)
    bool estasEnLaLimojDeLaSensilo = (s > 2 && s < 400);  // измерването в рамките на ограниченията на сензора ли е?
  
    // записване на резултата в глобалната променлива
    distanco = estasEnLaLimojDeLaSensilo ? s : -1; // запиши стойността, ако е валидна и -1 в противен случай
  }
}

void mezuruDistanconSeNecese() {
  // задаваме минималното време, през което искаме да се задейства ехолокация
  const unsigned long ULTRASONO_ELLASILON_INTERVALO = 1000; 

  // пресмятаме изминалото време от последната ехолокация до сега
  unsigned long nun = millis(); // времето от стартирането на програмата до сега (в милисекунди)
  unsigned long dt = nun - antaxuaUltrasonoEllasilonTempo; // изминалото време от последната ехолокация до сега

  // задействаме нова ехолокация, ако минималното време вече е изтекло
  if (dt >= ULTRASONO_ELLASILON_INTERVALO) {
    antaxuaUltrasonoEllasilonTempo = nun;
    sonar.ping_timer(mezuruDistancon); // тук подаваме функцията mezuruDistancon като аргумент на метода ping_timer ... магия :-S. Взех го също от https://bitbucket.org/teckel12/arduino-new-ping/wiki/Home, но не разбирам как работи, защото не съм проучвал как работят таймерите в Ардуино
    if (distanco == -1) {
      Serial.println("Разстоянието е извън обхвата на радара (от 2 до 400 сантиметра).");
    } else {
      Serial.println(distanco);
    }
  }
}

void setup() {
  // започни предаването по конзолата
  Serial.begin(9600);
  Serial.println("Стартиране на програмата");
}

void loop() {
  mezuruDistanconSeNecese();
}
