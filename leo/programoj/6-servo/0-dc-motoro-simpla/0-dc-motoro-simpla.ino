#include<Servo.h>

Servo motor;

void setup() {
  motor.attach(11);
  motor.write(0);
  for(int i = 0; i < 180; i += 10) {
    motor.write(i);
    delay(1000);
  }
  for(int i = 180; i >= 0; i -= 10) {
    motor.write(i);
    delay(1000);
  }
}

void loop() {}
