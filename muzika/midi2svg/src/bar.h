#ifndef BAR_HEADER_IS_ALREADY_INCLUDED
#define BAR_HEADER_IS_ALREADY_INCLUDED

struct Bar {
  int tsnum; // Time Signature Numerator
  int startTime;
  int minNoteNum;
  int maxNoteNum;
  int subSubDivisions;
  struct NoteNode* notesListPtr;
};

#endif