#ifndef UTILS_HEADER_IS_ALREADY_INCLUDED
#define UTILS_HEADER_IS_ALREADY_INCLUDED

#include<stdbool.h> // за bool

bool areEqual(char* a, char* b);

int gcd(int a, int b);

int translateNoteFromMidiToMyMusicalSystem(int midiNote);

int hue(int note);

int lightness(int note);

char* concat(char* s1, char* s2);

#endif