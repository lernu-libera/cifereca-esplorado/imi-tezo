// Спецификация на формата, който midicsv връща:
// https://www.fourmilab.ch/webtools/midicsv/#midicsv.5

#include<stdio.h> // за printf
#include<stdlib.h> // за malloc
#include<stdbool.h> // за bool
#include<string.h> // за strlen
#include"utils.h"
#include"note.h"
#include"notenode.h"
#include"song.h"

// 128 е обхватът на MIDI, а аз го изтеглям с още 3 ноти надолу, така че най-високата възможна нота в MIDI
// е с номер 131 в моята система ... да, това означава, че никога не можем да получим от входния MIDI файл
// най-ниските 3 ноти в моята система, но те така или иначе не могат да бъдат чути от никой човек по света
// повече обяснения по темата - малко по-долу
#define NOTES_RANGE_IN_MY_SCALE 131

void simplifyNoteTimes(struct Song* songPtr) {
  int timesGcd = findAllNotesGcd(songPtr->notesListPtr);
  divideAllNotesTimesByANumber(songPtr->notesListPtr, timesGcd);
  int pulsesPerBar = 4 * songPtr->ppq;
  int pulsesPerSectionInABar = pulsesPerBar / songPtr->tsdenom;
  songPtr->subSubDivisions = pulsesPerSectionInABar / timesGcd;
}

// http://www.music.mcgill.ca/~ich/classes/mumt306/midiformat.pdf
void parseHeader(char* line, struct Song* songPtr) {
  sscanf(line, "%*d, %*d, Header, %*d, %*d, %d\n", &(songPtr->ppq));
}

void parseTimeSignature(char* line, struct Song* songPtr) {
  int tsdenom; // знаменателят на тактовия размер, записан като отрицателна степен на числото две
  sscanf(line, "%*d, %*d, Time_signature, %d, %d, %*d, %*d\n", &(songPtr->tsnum), &tsdenom);
  
  // тъй като в MIDI не се записва наистина знаменателя, а се подразбира, че той ще е винаги степен на двойката
  // записват само степенния показател: 2 означава четвъртина, 3 е осмина, 4 - шестнадесетина, ...
  songPtr->tsdenom = 1 << tsdenom; // повдига denom като степенен на двойката
}

void parseTempo(char* line, struct Song* songPtr) {
  int tempo;
  sscanf(line, "%*d, %*d, Tempo, %d\n", &tempo);
  
  // темпото в MIDI се измерва в μs/(четвъртина нота), за да е със сигурност цяло число,
  // но музикантите го измерват в bpm = (четвъртини ноти)/минута
  songPtr->bpm = 60000000 / tempo;
}

void parseTitle(char* line, struct Song* songPtr) {
	songPtr->title = malloc(sizeof(char) * 100);
  sscanf(line, "%*d, %*d, Title_t, \"%[^\n\"]\"\n", songPtr->title);
}

void parseNoteOn(char* line, struct Song* songPtr) {
  int noteTime;
  int noteNum;
  sscanf(line, "%*d, %d, Note_on_c, %*d, %d, %*d\n", &noteTime, &noteNum);
  noteNum = translateNoteFromMidiToMyMusicalSystem(noteNum);
  songPtr->notesListPtr = addToEndOfList(songPtr->notesListPtr, initNote(noteNum, noteTime, -1));
}

void parseNoteOff(char* line, struct Song* songPtr) {
  int end;
  int noteNum;
  sscanf(line, "%*d, %d, Note_off_c, %*d, %d, %*d\n", &end, &noteNum);
  noteNum = translateNoteFromMidiToMyMusicalSystem(noteNum);
  struct Note* noteToBeClosedPtr = searchLastOpenNoteByNoteNumber(songPtr->notesListPtr, noteNum)->note;
  noteToBeClosedPtr->duration = end - noteToBeClosedPtr->start;
}

void parseLine(char* line, struct Song* songPtr) {
  char* type = malloc(sizeof(char) * 100);
  sscanf(line, "%*d, %*d, %[^,\n]", type);
  if (areEqual(type, "Header")) parseHeader(line, songPtr);
  else if (areEqual(type, "Time_signature")) parseTimeSignature(line, songPtr);
  else if (areEqual(type, "Tempo")) parseTempo(line, songPtr);
  else if (areEqual(type, "Title_t")) parseTitle(line, songPtr);
  else if (areEqual(type, "Note_on_c")) parseNoteOn(line, songPtr);
  else if (areEqual(type, "Note_off_c")) parseNoteOff(line, songPtr);
  free(type);
}

void printSong(struct Song* songPtr) {
  printf("Title: %s\n", songPtr->title);
  printf("Tempo: %d bpm\n", songPtr->bpm);
  printf("TimeSignature: %d/%d\n", songPtr->tsnum, songPtr->tsdenom);
  printf("SubSubDivisions: %d\n", songPtr->subSubDivisions);
  printAllNotes(songPtr->notesListPtr);
}

int main() {
	struct Song* songPtr = (struct Song*) calloc(1, sizeof(struct Song));
  
  char* line = malloc(sizeof(char) * 256);
  do {
    fgets(line, 255, stdin);
    parseLine(line, songPtr);
  } while(!feof(stdin));
  free(line);
  
	simplifyNoteTimes(songPtr);
	printSong(songPtr);
	
	// освободи паметта
	freeSongMemory(songPtr);
	
  return 0;
}
