#ifndef SONG_HEADER_IS_ALREADY_INCLUDED
#define SONG_HEADER_IS_ALREADY_INCLUDED

struct Song {
  int tsnum;
  int tsdenom;
  int bpm; // Beats per minute = Четвъртини / минута
  int ppq; // Clock Pulses per quarter note; quarter note = beat
  int subSubDivisions;
  struct NoteNode* notesListPtr; // a double-linked list with all notes in the song
  char* title;
};

// деструктор
void freeSongMemory(struct Song* songPtr);

struct NoteNode* extractNotesStartingBetween(struct Song* songPtr, int from, int to);

#endif