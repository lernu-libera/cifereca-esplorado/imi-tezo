#ifndef NOTE_HEADER_IS_ALREADY_INCLUDED
#define NOTE_HEADER_IS_ALREADY_INCLUDED

struct Note {
	int number; // номерът на нотата в моята скала: 6.875 Hz е с номер 0
  int start;
  int duration;
};

struct Note* initEmptyNote();
struct Note* initNote(int number, int start, int duration);

void freeNoteMemory(struct Note* notePtr);

#endif