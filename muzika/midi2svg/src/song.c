#include<stdio.h> // за printf
#include<stdlib.h> // за malloc
#include<stdbool.h> // за bool
#include"note.h"
#include"notenode.h"
#include"song.h"

struct NoteNode* extractNotesStartingBetween(struct Song* songPtr, int from, int to) {
	if (!songPtr || !songPtr->notesListPtr) return NULL;
	struct NoteNode* startNodePtr = firstNodePtr(songPtr->notesListPtr);
	
	// find the first note in the provided time boundaries
	struct NoteNode* noteNodesInBounds = NULL;
	while (startNodePtr) {
		int noteStartTime = startNodePtr->note->start;
		if (noteStartTime >= from && noteStartTime < to) {
			// this is a valid note. Take it out of the song and move it to the new list
			// but first, remember which is the next node after it, because it will be erased
			// the moment we remove this current node from the song.
			// The purpose of this erasing is to prevent cross-links from one list to the other and vice versa
			// which would make spaghetti of lists (a graph), and we do not want that.
			struct NoteNode* temp = startNodePtr->next;
			songPtr->notesListPtr = removeFromList(startNodePtr);
			if (!noteNodesInBounds) noteNodesInBounds = startNodePtr; // this is the first node in bounds
			else {
				// this is not the first node, attach it to the end of the list with the other valid notenodes
				struct NoteNode* lastValidNoteNodePtr = lastNodePtr(noteNodesInBounds);
				lastValidNoteNodePtr->next = startNodePtr;
				startNodePtr->prev = lastValidNoteNodePtr;
			}
			// get back to the next notenode in the song, so the loop can continue
			startNodePtr = temp;
		} else {
			startNodePtr = startNodePtr->next;
		}
	}
	
	return noteNodesInBounds;
}

// деструктор
void freeSongMemory(struct Song* songPtr) {
	freeTheWholeNoteNodeListMemory(songPtr->notesListPtr);
	free(songPtr);
}