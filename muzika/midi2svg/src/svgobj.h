#ifndef SVGOBJ_HEADER_IS_ALREADY_INCLUDED
#define SVGOBJ_HEADER_IS_ALREADY_INCLUDED

struct SvgObj {
  int width;
  int height;
  char* str;
};

struct SvgObj* createNoteSvg(int x, int y, struct Note* notePtr);

struct SvgObj* createBarSvg(int x, int y, struct Bar* barPtr);

struct SvgObj* createPianoRollSvg(int x, int y, struct Song* songPtr);

struct SvgObj* createSongSvg(int x, int y, struct Song* songPtr);

void printSvg(struct SvgObj* svgobj);

#endif