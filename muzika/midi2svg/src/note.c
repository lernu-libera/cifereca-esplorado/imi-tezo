#include<stdlib.h> // за malloc, calloc и free
#include"note.h"

// конструктор
// не успях да overload-на долния конструктор ... четох как се прави в C, но ми се стори много сложно
// затова избрах различно име
struct Note* initEmptyNote() {
	struct Note* note = (struct Note*) calloc(1, sizeof(struct Note)); // Clearly allocate 1 Note (initialize with 0)
	return note;
}

// конструктор
struct Note* initNote(int number, int start, int duration) {
	struct Note* note = initEmptyNote();
	note->number = number;
	note->start = start;
	note->duration = duration;
	return note;
}

// деструктор
void freeNoteMemory(struct Note* notePtr) {
	free(notePtr);
}