#include<stdio.h> // за printf
#include<stdlib.h> // за malloc
#include<stdbool.h> // за bool
#include<string.h> // за strlen
#include<math.h> // за ceil
#include"utils.h"
#include"note.h"
#include"notenode.h"
#include"bar.h"
#include"song.h"
#include"svgobj.h"

void parseNote(char* line, struct Song* songPtr) {
	int num;
	int start;
	int duration;
	sscanf(line, "Note: %d,%d,%d\n", &num, &start, &duration);
	songPtr->notesListPtr = addToEndOfList(songPtr->notesListPtr, initNote(num, start, duration));
}

void parseLine(char* line, struct Song* songPtr) {
  char type[100];
  sscanf(line, "%[^:]: %*[^\n]\n", &type);
  //printf("%s\n", line);
  if (areEqual(type, "Title")) {
  	songPtr->title = malloc(sizeof(char) * 100);
  	sscanf(line, "Title: %[^\n]\n", songPtr->title);
  }
  else if (areEqual(type, "Tempo")) sscanf(line, "Tempo: %d bpm\n", &(songPtr->bpm));
  else if (areEqual(type, "TimeSignature")) sscanf(line, "TimeSignature: %d/%d\n", &(songPtr->tsnum), &(songPtr->tsdenom));
  else if (areEqual(type, "SubSubDivisions")) sscanf(line, "SubSubDivisions: %d\n", &(songPtr->subSubDivisions));
  else if (areEqual(type, "Note")) parseNote(line, songPtr);
}

int main() {
  struct Song* songPtr = (struct Song*) calloc(1, sizeof(struct Song));
  
  char line[256];
  do {
    fgets(line, sizeof(line), stdin);
    parseLine(line, songPtr);
  } while(!feof(stdin));
  
	struct SvgObj* ret = createPianoRollSvg(20, 20, songPtr);
	
  printSvg(ret);
  
  // освободи паметта
	free(songPtr);
  
  return 0;
}