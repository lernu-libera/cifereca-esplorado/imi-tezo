#include<stdlib.h> // за malloc, calloc и free
#include<stdio.h> // за printf
#include"note.h"
#include"notenode.h"
#include"bar.h"
#include"song.h"
#include"svgobj.h"
#include"utils.h"

#define LINE_WIDTH_4 8
#define LINE_WIDTH_3 4
#define LINE_WIDTH_2 2
#define LINE_WIDTH_1 1
#define MARGINX 20
#define MARGINY 20
#define NOTE_SQUARE_SIDE 10

struct SvgObj* createNoteSvg(int x, int y, struct Note* notePtr) {
	char* str;
	int w = notePtr->duration*NOTE_SQUARE_SIDE;
	int h = NOTE_SQUARE_SIDE;
	int hu = hue(notePtr->number);
	int li = lightness(notePtr->number);
	
	
	asprintf(&str,"      <!-- нота --><rect x=\"%d\" y=\"%d\" width=\"%d\" height=\"%d\" fill=\"hsl(%d, %d%, %d%)\" stroke=\"black\" stroke-width=\"%d\" />\n        <!-- надпис на нота --><text x=\"%d\" y=\"%d\" fill=\"black\" font-size=\"0.4em\" dominant-baseline=\"middle\" text-anchor=\"middle\">%d</text>\n", x, y, w, h, hu, 40, li, LINE_WIDTH_1, x + w/2, y + h/2, notePtr->number%12);
	
	free(notePtr);
	
	struct SvgObj* ret = (struct SvgObj*) calloc(1, sizeof(struct SvgObj));
	ret->width = w;
	ret->height = h;
	ret->str = str;
	return ret;
}

struct SvgObj* createBarSvg(int x, int y, struct Bar* barPtr) {
	if (!barPtr) return NULL;
	if (!barPtr->notesListPtr) {
		free(barPtr);
		return NULL;
	}
	
	// the width and height of the bar must not depend on the notes inside it,
	// otherwise, bars who are not completely full with notes will appear thinner
	// and those with smaller notes pitch distance will appear shorter
	int w = barPtr->tsnum * barPtr->subSubDivisions * NOTE_SQUARE_SIDE;
	int h = (barPtr->maxNoteNum - barPtr->minNoteNum + 1)*NOTE_SQUARE_SIDE;
	
	int left = x;
	int right = x;
	int top = y;
	int bottom = y;
	
	struct SvgObj* ret = (struct SvgObj*) calloc(1, sizeof(struct SvgObj));
	asprintf(&(ret->str),"    <!-- такт -->\n");
	
	// print vertical lines
	for (int i = 1; i < barPtr->tsnum * barPtr->subSubDivisions; i++) {
		char* lineStr;
		int lineX = x + i*NOTE_SQUARE_SIDE;
		int lineWidth = i % barPtr->subSubDivisions ? LINE_WIDTH_1 : LINE_WIDTH_2;
		asprintf(&lineStr,"      <!-- вертикална линия в такта --><line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" stroke=\"black\" stroke-width=\"%d\" />\n", lineX, y, lineX, y + h, lineWidth);
		ret->str = concat(ret->str, lineStr);
	}
	
	struct NoteNode* startNodePtr = firstNodePtr(barPtr->notesListPtr);
	while (startNodePtr) {
		struct Note* note = startNodePtr->note;
		int noteLeft = x + (note->start - barPtr->startTime)*NOTE_SQUARE_SIDE;
		int noteTop = y + (barPtr->maxNoteNum - note->number)*NOTE_SQUARE_SIDE;
		struct SvgObj* noteSvg = createNoteSvg(noteLeft, noteTop, note);
		ret->str = concat(ret->str, noteSvg->str);
		free(noteSvg);
		struct NoteNode* temp = startNodePtr->next;
		free(startNodePtr);
		startNodePtr = temp;
	}
	ret->width = w;
	ret->height = h;
	
	// print a horizontal line for each note
	for (int i = barPtr->minNoteNum; i < barPtr->maxNoteNum; i++) {
		char* lineStr;
		int lineY = y + (barPtr->maxNoteNum - i)*NOTE_SQUARE_SIDE;
		int lineWidth = (i + 1) % 12 ? (i + 1) % 3 ? LINE_WIDTH_1 : LINE_WIDTH_2 : LINE_WIDTH_3;
		asprintf(&lineStr,"      <!-- хоризонтална линия в такта --><line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" stroke=\"black\" stroke-width=\"%d\" />\n", x, lineY, x + w, lineY, lineWidth);
		ret->str = concat(ret->str, lineStr);
	}
	
	free(barPtr);

	char* borderStr;
	asprintf(&borderStr,"      <!-- рамка на такт --><rect x=\"%d\" y=\"%d\" width=\"%d\" height=\"%d\" fill-opacity=\"0.0\" stroke=\"black\" stroke-width=\"%d\" />\n", x, y, w, h, LINE_WIDTH_3);
	ret->str = concat(ret->str, borderStr);

	return ret;
}

struct SvgObj* createPianoRollSvg(int x, int y, struct Song* songPtr) {
	if (!songPtr || !songPtr->notesListPtr) return NULL;
	struct NoteNode* startNodePtr = firstNodePtr(songPtr->notesListPtr);
	
	int left = x;
	int right = x;
	int top = y;
	int bottom = y;
	
	struct SvgObj* ret = (struct SvgObj*) calloc(1, sizeof(struct SvgObj));
	asprintf(&(ret->str),"  <!-- пиано рол -->\n");
  
  struct Bar* barPtr;  
  int barStartTime = 0;
  int lowestNote = lowestNoteNum(songPtr->notesListPtr);
  int highestNote = highestNoteNum(songPtr->notesListPtr);
  // loop while there are any notes left in the song
  while(songPtr->notesListPtr) {
  	int barEndTime = barStartTime + songPtr->tsnum * songPtr->subSubDivisions;
  	barPtr = (struct Bar*) malloc(sizeof(struct Bar));
  	barPtr->tsnum = songPtr->tsnum;
  	barPtr->minNoteNum = lowestNote;
		barPtr->maxNoteNum = highestNote;
		barPtr->subSubDivisions = songPtr->subSubDivisions;
		barPtr->startTime = barStartTime;
		barPtr->notesListPtr = extractNotesStartingBetween(songPtr, barStartTime, barEndTime);
		struct SvgObj* svgobj = createBarSvg(right, y, barPtr);
		if (svgobj) {
			ret->str = concat(ret->str, svgobj->str);
			right += svgobj->width; // they are attached one after another
			bottom = svgobj->height; // they are the same
			free(svgobj);
		}
		barStartTime = barEndTime; // increase the bar time to match the next bar, for the next iteration
	}
	
	ret->width = right - left;
	ret->height = bottom - top;
	
	return ret;
}

struct SvgObj* createSongSvg(int x, int y, struct Song* songPtr) {
	int tsnum; // Time Signature Numerator
  int startTime;
  int minNoteNum;
  int maxNoteNum;
  int subSubDivisions;
  struct NoteNode* notesListPtr;
	
	struct Bar* barPtr = (struct Bar*) malloc(sizeof(struct Bar));
	barPtr->tsnum = songPtr->tsnum;
	barPtr->minNoteNum = lowestNoteNum(songPtr->notesListPtr);
	barPtr->maxNoteNum = highestNoteNum(songPtr->notesListPtr);
	barPtr->subSubDivisions = songPtr->subSubDivisions;
	
	// draw the first bar
	int minx = x;
	int maxx = x;
	int miny = y;
	int maxy = y;
	
	int left = x;
	int right = x;
	int top = y;
	int bottom = y;
	
	barPtr->startTime = 16;
	barPtr->notesListPtr = extractNotesStartingBetween(songPtr, barPtr->startTime, barPtr->startTime + barPtr->tsnum * barPtr->subSubDivisions - 1);
	struct SvgObj* svgobj = createBarSvg(x, y, barPtr);
	right = x + svgobj->width;
	bottom = y + svgobj->height;
	if (right > maxx) maxx = right;
	if (bottom > maxy) maxy = bottom;
	
	//barPtr->startTime = 16;
	//barPtr->notesListPtr = extractNotesStartingBetween(songPtr, barPtr->startTime, barPtr->startTime + barPtr->tsnum * barPtr->subSubDivisions - 1);
	//struct SvgObj* temp = createBarSvg(x, y, barPtr);
	//right = x + temp->width;
	//bottom = y + temp->height;
	//if (right > maxx) maxx = right;
	//if (bottom > maxy) maxy = bottom;
	//svgobj->str = concat(svgobj->str, temp->str);
	
	int w = maxx - minx;
	int h = maxy - miny;
	
	printf("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
	printf("<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n");
	printf("<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"%d\" height=\"%d\">\n", w, h);
	printf("  <!-- рамка -->\n");
  printf("%s", svgobj->str);
  free(svgobj->str);
  free(svgobj);
	printf("</svg>\n");
}

void printSvg(struct SvgObj* svgobj) {
	if (!svgobj) return;
	int w = svgobj->width + 2*MARGINX;
	int h = svgobj->height + 2*MARGINY;
	printf("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
	printf("<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n");
	printf("<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" width=\"%d\" height=\"%d\">\n", w, h);
	printf("  <!-- рамка -->\n");
  printf("%s", svgobj->str);
	free(svgobj->str);
  free(svgobj);
	printf("</svg>\n");
}