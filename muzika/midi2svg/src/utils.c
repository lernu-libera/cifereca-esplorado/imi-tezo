#include<stdlib.h> // за malloc, calloc и free
#include<stdbool.h> // за bool
#include<string.h> // за strlen, strcpy, strcat

#include"utils.h"

char* concat(char* s1, char* s2) {
  char* result = malloc(strlen(s1) + strlen(s2) + 1);
  strcpy(result, s1);
  strcat(result, s2);
  free(s1);
  free(s2);
  return result;
}

bool areEqual(char* a, char* b) {
  return !strcmp(a, b);
}

// алгоритъмът на Евклид, чрез рекурсия
// https://en.wikipedia.org/wiki/Euclidean_algorithm
int gcd(int a, int b) {
  return b == 0 ? a : gcd(b, a%b);
}

int translateNoteFromMidiToMyMusicalSystem(int midiNote) {
  // https://www.inspiredacoustics.com/en/MIDI_note_numbers_and_center_frequencies
  // нота 0 в MIDI е C (до) на -1 октава, 8.18 Hz
  // нота 0 в моята система е 3 ноти по-ниско: нота A (ла), 6.875 Hz.
  // От нея започва моята нулева октава, а не от нотата C.
  //
  // https://en.wikipedia.org/wiki/Hearing_range
  // Най-ниската честота, която средностатистическия човек може да чуе е 20 Hz.
  // В идеални лабораторни условия, човек с много добър слух може да чуе 12 Hz.
  // Приблизително 12.25 Hz е нотата G на -1 октава в общоприетата система (Equal temperament).
  // В моята система избирам тази октава за нулева, с което избягвам употребата на отрицателни индекси
  // за обхвата от честоти, който човекът с най-добрия възможен слух би могъл да чуе.
  // За целите на музиката, не считам за нужно да дефинирам ноти, които хората не можем да чуем,
  // а ако искаме да дефинираме чрез моята система честоти по-ниски от чуваемите за нас,
  // вече можем да ползваме отрицателни индекси, но тогава отрицателният индекс поставя нотата в контекст,
  // който настоящата ни система приписва и на част от чуваемите от хората честоти.
  //
  // https://en.wikipedia.org/wiki/Equal_temperament
  // Нотата A (ла) избирам за базова за системата. Не нотата C, както е в MIDI,
  // защото по съвременния стандарт за настройване на музикални инструменти нотата A4 = 440 Hz е базовата нота.
  // и всички останали ноти се получават от нея по формула.
  // При моята система, 440 Hz е нотата A6 октава, заради променените индекси, за които обясних по-горе.
  // При тези промени получаването на честотата на нота става вече много лесно, ако ползваме за базова нота
  // нотата A0 от моята система = 6.875 Hz, която е точно 6 октави под общоприетата за базова нота A (ла) 440Hz:
  // 440/(2^6) Hz = 6.875 Hz
  // Тогава формулата за честотата на всяка нота става:
  // честота на нотата = 6.875 * (2 ^ (номер на нотата / 12))
  return midiNote + 3;
}

int hue(int note) {
  int noteInOctave = note % 12;
  switch(noteInOctave) {
    case 0:  return 45; // A
    case 1: return 60; // A#
    case 2: return 87; // B
    case 3:  return 138; // C
    case 4:  return 164; // C#
    case 5:  return 224; // D
    case 6:  return 264; // D#
    case 7:  return 300; // E
    case 8:  return 322; // F
    case 9:  return 5; // F#
    case 10:  return 22; // G
    case 11:  return 30; // G#
  }
}

int lightness(int note) {
  int octave = note / 12;
  switch(octave) {
    case 4: return 35;
    case 5: return 55;
    case 6: return 80;
    default: return 100;
  }
}