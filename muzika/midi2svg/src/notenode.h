#ifndef NOTENODE_HEADER_IS_ALREADY_INCLUDED
#define NOTENODE_HEADER_IS_ALREADY_INCLUDED

// възел за изграждане на свързан списък от ноти
struct NoteNode {
  struct Note* note;
  struct NoteNode* prev;
  struct NoteNode* next;
};

struct NoteNode* initEmptyNoteNode();
struct NoteNode* initNoteNode(int number, int start, int duration);

struct NoteNode* searchLastOpenNoteByNoteNumber(struct NoteNode* startNodePtr, int noteNumber);

struct NoteNode* firstNodePtr(struct NoteNode* startNodePtr);

// get a pointer to the last NoteNode in the linked list
struct NoteNode* lastNodePtr(struct NoteNode* startNodePtr);

// enqueue a note to the linked list
// the node is allocated in the heep and has to be freed later
// if NULL was received a new list is created
struct NoteNode* addToEndOfList(struct NoteNode* startNodePtr, struct Note* noteToBeAddedPtr);

struct NoteNode* removeFromList(struct NoteNode* startNodePtr);

struct NoteNode* moveToList(struct NoteNode* nodeToMove, struct NoteNode* listToMoveItTo);

struct NoteNode* freeNoteNodeMemory(struct NoteNode* nodePtr);

// frees the memory which was allocated for every Node and Note in the list
void freeTheWholeNoteNodeListMemory(struct NoteNode* startNodePtr);

void printAllNotes(struct NoteNode* startNodePtr);

int findAllNotesGcd(struct NoteNode* startNodePtr);

int lowestNoteNum(struct NoteNode* startNodePtr);

int highestNoteNum(struct NoteNode* startNodePtr);

void divideAllNotesTimesByANumber(struct NoteNode* startNodePtr, int divisor);

#endif