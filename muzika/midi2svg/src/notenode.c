#include<stdlib.h> // за malloc, calloc и free
#include<stdio.h> // за printf
#include"note.h"
#include"notenode.h"
#include"utils.h"

#define NOTES_RANGE_IN_MY_SCALE 131

// конструктор
// не успях да overload-на долния конструктор
struct NoteNode* initEmptyNoteNode() {
	struct NoteNode* noteNode = (struct NoteNode*) calloc(1, sizeof(struct NoteNode));
	return noteNode;
}

// конструктор
struct NoteNode* initNoteNode(int number, int start, int duration) {
	struct Note* note = initNote(number, start, duration);
	struct NoteNode* noteNode = initEmptyNoteNode();
	noteNode->note = note;
	return noteNode;
}

struct NoteNode* searchLastOpenNoteByNoteNumber(struct NoteNode* startNodePtr, int noteNumber) {
	startNodePtr = lastNodePtr(startNodePtr);
	while (startNodePtr) {
	 	if (startNodePtr->note->number == noteNumber && startNodePtr->note->duration == -1) return startNodePtr;
		startNodePtr = startNodePtr->prev;
	}
}

// a function which returns a pointer to the first NoteNode in the double-linked list
struct NoteNode* firstNodePtr(struct NoteNode* startNodePtr) {
	if (!startNodePtr) return NULL;
	while (startNodePtr->prev) startNodePtr = startNodePtr->prev;
	return startNodePtr;
}

// a recursive function which returns a pointer to the last NoteNode in the linked list
struct NoteNode* lastNodePtr(struct NoteNode* startNodePtr) {
	if (!startNodePtr) return NULL;
	if (!startNodePtr->next) return startNodePtr;
	return lastNodePtr(startNodePtr->next);
}

struct NoteNode* addToEndOfList(struct NoteNode* startNodePtr, struct Note* noteToBeAddedPtr) {
	struct NoteNode* noteNode = initEmptyNoteNode();
	noteNode->note = noteToBeAddedPtr;
	if (!startNodePtr) return noteNode;
	struct NoteNode* lastNodePtrInTheInputList = lastNodePtr(startNodePtr);
	lastNodePtrInTheInputList->next = noteNode;
	noteNode->prev = lastNodePtrInTheInputList;
	return startNodePtr;
}

// returns a pointer to the next element in the list if there is such
// if there isn`t, return a pointer to the previous
// if there is no previous either, returns NULL
// does not free the memory, because the removed element might be used in another list
struct NoteNode* removeFromList(struct NoteNode* noteToBeRemoved) {
	if (!noteToBeRemoved) return NULL; // подаден е NULL, върни NULL
	
	// запази указатели към предишния и следващия елемент, за да работим с тях по-късно
	struct NoteNode* prev = noteToBeRemoved->prev;
	struct NoteNode* next = noteToBeRemoved->next;
	
	// върни NULL, ако не е имало нито следващ, нито предишен елемент
	if (!next && !prev) return NULL;
	
	// премахни препратките към тях от елемента, който ще махаме, за да не се станат кръстосани препратки между списъци
	// ако този елемент се ползва в други списъци по-късно
	noteToBeRemoved->prev = NULL;
	noteToBeRemoved->next = NULL;
	
	// махни препратките към него от предишния и следващия елемент в списъка, за онези от тях, които ги е имало
	if (prev) prev->next = next;
	if (next) next->prev = prev;
	
	// върни следващия елемент, ако е имало такъв или предишния, ако не е имало следващ
	if (next) return next;
	return prev;
}

struct NoteNode* moveToList(struct NoteNode* nodeToMove, struct NoteNode* listToMoveItTo) {
	struct NoteNode* ret = removeFromList(nodeToMove);
	struct NoteNode* last = lastNodePtr(listToMoveItTo);
	last->next = nodeToMove;
	nodeToMove->prev = last;
	return ret;
}

// destructor
// removes the node from its list and frees the memory
struct NoteNode* freeNoteNodeMemory(struct NoteNode* noteNodePtr) {
	if (!noteNodePtr) return NULL; // подаден е NULL, върни NULL
	struct NoteNode* ret = removeFromList(noteNodePtr);
	freeNoteMemory(noteNodePtr->note);
	free(noteNodePtr);
	return ret;
}

void freeTheWholeNoteNodeListMemory(struct NoteNode* startNodePtr) {
	if (!startNodePtr) return;
	freeTheWholeNoteNodeListMemory(freeNoteNodeMemory(startNodePtr));
}

void printAllNotes(struct NoteNode* startNodePtr) {
	if (!startNodePtr) return;
	startNodePtr = firstNodePtr(startNodePtr);
	while (startNodePtr) {
		printf("Note: %d,%d,%d\n", startNodePtr->note->number, startNodePtr->note->start, startNodePtr->note->duration);
		startNodePtr = startNodePtr->next;
	}
}

// returns the GCD of all note times
int findAllNotesGcd(struct NoteNode* startNodePtr) {
	if (!startNodePtr) return 0;
	
	startNodePtr = firstNodePtr(startNodePtr); // go to the first note in the list, in case the argument is not it
	int timesGcd = startNodePtr->note->start; //printAllNotes(startNodePtr);
	do {
		timesGcd = gcd(timesGcd, startNodePtr->note->start);
    timesGcd = gcd(timesGcd, startNodePtr->note->duration);
    startNodePtr = startNodePtr->next;
	} while(startNodePtr);
	
	return timesGcd;
}

int lowestNoteNum(struct NoteNode* startNodePtr) {
	if (!startNodePtr) return -1;
	int min = NOTES_RANGE_IN_MY_SCALE; // set the initial minimum note to the maximum possible in my scale
	startNodePtr = firstNodePtr(startNodePtr); // go to the first note in the list, in case the argument is not it
	do {
		int noteNum = startNodePtr->note->number;
		if (noteNum < min) min = noteNum;
    startNodePtr = startNodePtr->next;
	} while(startNodePtr);
	return min;
}

int highestNoteNum(struct NoteNode* startNodePtr) {
	if (!startNodePtr) return -1;
	int max = 0;
	startNodePtr = firstNodePtr(startNodePtr); // go to the first note in the list, in case the argument is not it
	do {
		int noteNum = startNodePtr->note->number;
		if (noteNum > max) max = noteNum;
    startNodePtr = startNodePtr->next;
	} while(startNodePtr);
	return max;
}

void divideAllNotesTimesByANumber(struct NoteNode* startNodePtr, int divisor) {
	do {
		startNodePtr->note->start /= divisor;
    startNodePtr->note->duration /= divisor;
    startNodePtr = startNodePtr->next;
	} while(startNodePtr);
}
